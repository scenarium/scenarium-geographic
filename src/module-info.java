import io.scenarium.geographic.Plugin;
import io.scenarium.pluginManager.PluginsSupplier;

open module io.scenarium.geographic {
	uses PluginsSupplier;

	provides PluginsSupplier with Plugin;

	exports io.scenarium.geographic;
	exports io.scenarium.geographic.operator.viewer.conversion;
	exports io.scenarium.geographic.drawer;
	exports io.scenarium.geographic.drawer.geo;
	exports io.scenarium.geographic.editor;
	exports io.scenarium.geographic.internal;
	exports io.scenarium.geographic.struct;

	requires transitive io.scenarium.gui.flow;
	requires javafx.graphics;
	requires javafx.swing;
	requires ejml.ddense;
	requires ejml.core;
}
