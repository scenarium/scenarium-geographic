/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.geographic.struct;

import java.util.function.Function;

import com.jogamp.opengl.GL2;

import javafx.geometry.Point2D;
import javafx.scene.paint.Color;

public interface GeographicalDrawableObject3D {
	public void draw(GL2 gl, Color color, Function<GeographicCoordinate, Point2D> geoToOpenGL);
}
