/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.geographic.struct;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Objects;

public class GeographicCoordinate implements Cloneable, Serializable {
	private static final long serialVersionUID = 1L;
	protected static double earthRadius = 6378137;

	/** Gets the distance using Haversine formula.
	 *
	 * @param la1 the Latitude in degrees
	 * @param lo1 the Longitude in degrees
	 * @param la2 the Latitude from 2nd coordinate in degrees
	 * @param lo2 the Longitude from 2nd coordinate in degrees
	 * @return the distance */
	public static double getDistanceHaversine(double la1, double lo1, double la2, double lo2) {
		double aStartLat = Math.toRadians(la1);
		double aStartLong = Math.toRadians(lo1);
		double aEndLat = Math.toRadians(la2);
		double aEndLong = Math.toRadians(lo2);
		double dLongitude = aEndLong - aStartLong;
		double dLatitude = aEndLat - aStartLat;
		double l = Math.sin(dLatitude / 2.0);
		double m = Math.sin(dLongitude / 2.0);
		double a = l * l + Math.cos(aStartLat) * Math.cos(aEndLat) * m * m;
		double c = 2.0 * Math.asin(Math.sqrt(a));
		return earthRadius * c;
	}

	/** Gets the distance using Spherical law of cosines.
	 *
	 * @param la1 the Latitude in degrees
	 * @param lo1 the Longitude in degrees
	 * @param la2 the Latitude from 2nd coordinate in degrees
	 * @param lo2 the Longitude from 2nd coordinate in degrees
	 * @return the distance */
	public static double getDistanceLawOfCosines(double la1, double lo1, double la2, double lo2) {
		double aStartLat = Math.toRadians(la1);
		double aStartLong = Math.toRadians(lo1);
		double aEndLat = Math.toRadians(la2);
		double aEndLong = Math.toRadians(lo2);
		double distance = Math.acos(Math.sin(aStartLat) * Math.sin(aEndLat) + Math.cos(aStartLat) * Math.cos(aEndLat) * Math.cos(aEndLong - aStartLong));
		return earthRadius * distance;
	}

	public final double latitude;
	public final double longitude;
	public final double altitude;

	// public final double stdLatitude;
	// public final double stdLongitude;
	public final double stdAltitude;

	public final double[] posVar;

	public GeographicCoordinate(double latitude, double longitude) {
		this(latitude, longitude, Double.NaN);
	}

	public GeographicCoordinate(double latitude, double longitude, double altitude) {
		this(latitude, longitude, altitude, null, Double.NaN);
	}

	public GeographicCoordinate(double latitude, double longitude, double altitude, double[] posVar, double stdAltitude) {
		this.latitude = latitude;
		this.longitude = longitude;
		this.altitude = altitude;
		this.posVar = posVar;
		this.stdAltitude = stdAltitude;
	}

	public GeographicCoordinate(double latitude, double longitude, double altitude, double stdLatitude, double stdLongitude, double stdAltitude) {
		this(latitude, longitude, altitude, new double[] { stdLatitude, 0, 0, stdLongitude }, stdAltitude);
	}

	@Override
	public GeographicCoordinate clone() {
		return new GeographicCoordinate(this.latitude, this.longitude, this.altitude, this.posVar, this.stdAltitude);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (!(obj instanceof GeographicCoordinate))
			return false;
		if (obj == this)
			return true;
		GeographicCoordinate gc = (GeographicCoordinate) obj;

		return Double.doubleToLongBits(this.latitude) == Double.doubleToLongBits(gc.latitude) && Double.doubleToLongBits(this.longitude) == Double.doubleToLongBits(gc.longitude)
				&& Double.doubleToLongBits(this.altitude) == Double.doubleToLongBits(gc.altitude) && Arrays.equals(this.posVar, gc.posVar)
				&& Double.doubleToLongBits(this.stdAltitude) == Double.doubleToLongBits(gc.stdAltitude);
	}

	public double getDistanceHaversine(double lat, double lon) {
		return getDistanceHaversine(this.latitude, this.longitude, lat, lon);
	}

	public double getDistanceHaversine(GeographicCoordinate geographicCoordinate) {
		return getDistanceHaversine(this.latitude, this.longitude, geographicCoordinate.latitude, geographicCoordinate.longitude);
	}

	public double getDistanceLawOfCosines(double lat, double lon) {
		return getDistanceLawOfCosines(this.latitude, this.longitude, lat, lon);
	}

	public double getDistanceLawOfCosines(GeographicCoordinate geographicCoordinate) {
		return getDistanceLawOfCosines(this.latitude, this.longitude, geographicCoordinate.latitude, geographicCoordinate.longitude);
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.latitude, this.longitude, this.altitude, this.posVar, this.stdAltitude);
	}

	/** Increment meters to a geographical coordinate
	 *
	 * @param dx the number of meters along the x axis
	 * @param dy the number of meters along the y axis
	 * @return the new incremented geographical coordinate */
	public GeographicCoordinate incrementFast(double dx, double dy) {
		return new GeographicCoordinate(this.latitude + dy / earthRadius * (180 / Math.PI), this.longitude + dx / earthRadius * (180 / Math.PI) / Math.cos(this.latitude * Math.PI / 180),
				this.altitude, this.posVar, this.stdAltitude);
	}

	@Override
	public String toString() {
		return "GeographicCoordinate [latitude=" + this.latitude + ", longitude=" + this.longitude + (!Double.isNaN(this.altitude) ? ", altitude=" + this.altitude : "")
				+ (this.posVar != null ? ", posVar=" + this.posVar : "") + (!Double.isNaN(this.stdAltitude) ? ", stdAltitude=" + this.stdAltitude + "]" : "]");
	}
}
