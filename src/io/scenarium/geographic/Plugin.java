package io.scenarium.geographic;

import java.io.InputStream;

import io.beanmanager.PluginsBeanSupplier;
import io.beanmanager.consumer.ClassNameRedirectionConsumer;
import io.beanmanager.consumer.EditorConsumer;
import io.scenarium.flow.consumer.ClonerConsumer;
import io.scenarium.flow.consumer.OperatorConsumer;
import io.scenarium.flow.consumer.PluginsFlowSupplier;
import io.scenarium.geographic.drawer.GeographicalDrawer;
import io.scenarium.geographic.drawer.geo.CrossMarker;
import io.scenarium.geographic.editor.GeographicCoordinateEditor;
import io.scenarium.geographic.editor.GeographicalPoseEditor;
import io.scenarium.geographic.internal.LoadPackageStream;
import io.scenarium.geographic.operator.viewer.conversion.ToCrossMarker;
import io.scenarium.geographic.struct.GeographicCoordinate;
import io.scenarium.geographic.struct.GeographicalPose;
import io.scenarium.gui.core.PluginsGuiCoreSupplier;
import io.scenarium.gui.core.consumer.DrawersConsumer;
import io.scenarium.pluginManager.PluginsSupplier;

public class Plugin implements PluginsSupplier, PluginsFlowSupplier, PluginsBeanSupplier, PluginsGuiCoreSupplier {

	@Override
	public void populateCloners(ClonerConsumer clonerConsumer) {
		clonerConsumer.accept(GeographicCoordinate.class, GeographicCoordinate::clone);
		clonerConsumer.accept(GeographicalPose.class, GeographicalPose::clone);
		clonerConsumer.accept(CrossMarker.class, CrossMarker::clone);
	}

	@Override
	public void populateClassNameRedirection(ClassNameRedirectionConsumer classNameRedirectionConsumer) {
		classNameRedirectionConsumer.accept("io.scenarium.core.operator.network.decoder.NMEA0183Decoder", "io.scenarium.nmea.operator.network.decoder.NMEA0183Decoder");
	}

	@Override
	public void populateEditors(EditorConsumer editorConsumer) {
		editorConsumer.accept(GeographicCoordinate.class, GeographicCoordinateEditor.class);
		editorConsumer.accept(GeographicalPose.class, GeographicalPoseEditor.class);
	}

	@Override
	public void populateDrawers(DrawersConsumer drawersConsumer) {
		drawersConsumer.accept(GeographicCoordinate.class, GeographicalDrawer.class);
	}

	@Override
	public void populateOperators(OperatorConsumer operatorConsumer) {
		operatorConsumer.accept(ToCrossMarker.class);
	}
	
	public static InputStream getLocalFileStream(String fileName) {
		return LoadPackageStream.getStream(fileName);
	}
}
