/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.geographic.drawer;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;

import javax.imageio.ImageIO;
import javax.vecmath.Point2d;

import io.beanmanager.editors.PropertyInfo;
import io.beanmanager.editors.container.ArrayInfo;
import io.beanmanager.editors.primitive.number.ControlType;
import io.beanmanager.editors.primitive.number.NumberInfo;
import io.beanmanager.struct.BooleanProperty;
import io.beanmanager.struct.TreeNode;
import io.beanmanager.struct.TreeRoot;
import io.scenarium.core.struct.ScenariumProperties;
import io.scenarium.core.timescheduler.Scheduler;
import io.scenarium.geographic.drawer.geo.BingAerialTileSource;
import io.scenarium.geographic.drawer.geo.CycleMapTileSource;
import io.scenarium.geographic.drawer.geo.GoogleHybridTileSource;
import io.scenarium.geographic.drawer.geo.GoogleRoadsOnlyTileSource;
import io.scenarium.geographic.drawer.geo.GoogleSatelliteOnlyTileSource;
import io.scenarium.geographic.drawer.geo.GoogleSomehowAlteredRoadmapTileSource;
import io.scenarium.geographic.drawer.geo.GoogleStandardRoadmapTileSource;
import io.scenarium.geographic.drawer.geo.GoogleTerrainOnlyTileSource;
import io.scenarium.geographic.drawer.geo.GoogleTerrainTileSource;
import io.scenarium.geographic.drawer.geo.LandscapeTileSource;
import io.scenarium.geographic.drawer.geo.MapQuestOSMAerialTileSource;
import io.scenarium.geographic.drawer.geo.MapQuestOSMTileSource;
import io.scenarium.geographic.drawer.geo.MapnikTileSource;
import io.scenarium.geographic.drawer.geo.MappyPhotoTileSource;
import io.scenarium.geographic.drawer.geo.MappyStandardTileSource;
import io.scenarium.geographic.drawer.geo.OsmMercator;
import io.scenarium.geographic.drawer.geo.TileSource;
import io.scenarium.geographic.internal.LoadPackageStream;
import io.scenarium.geographic.internal.Log;
import io.scenarium.geographic.struct.GeographicCoordinate;
import io.scenarium.geographic.struct.GeographicalDrawableObject2D;
import io.scenarium.geographic.struct.GeographicalPose;
import io.scenarium.gui.core.display.ColorProvider;
import io.scenarium.gui.core.display.ScenariumContainer;
import io.scenarium.gui.core.display.StackableDrawer;
import io.scenarium.gui.core.display.drawer.TheaterPanel;
import io.scenarium.gui.core.display.drawer.TileId;

import org.ejml.data.DMatrix2x2;
import org.ejml.data.DMatrixRMaj;
import org.ejml.dense.row.factory.DecompositionFactory_DDRM;
import org.ejml.interfaces.decomposition.EigenDecomposition_F64;

import javafx.embed.swing.SwingFXUtils;
import javafx.geometry.Dimension2D;
import javafx.geometry.Point2D;
import javafx.scene.Cursor;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.scene.transform.Affine;

public class GeographicalDrawer extends TheaterPanel implements StackableDrawer {
	private static final int MAX_MERCATOR_TILE_ZOOM = 22;
	protected static final String GENERAL_FILTERS = "General";
	protected static final String BORDER = "Border";
	protected static final String GRID = "Grid";
	private static final String MAPNIK = "Mapnik";
	private static final String OSM_CYCLE_MAPS = "OSM Cycle Maps";
	private static final String BING_AERIAL_MAPS = "Bing Aerial Maps";
	private static final String MAPQUEST_OSM = "MapQuest-OSM";
	private static final String MAPQUEST_OPEN_AERIAL = "MapQuest Open Aerial";
	private static final String GOOGLE_ROADS_SONLY = "Google Roads Only";
	private static final String GOOGLE_STANDARD_ROADMAP = "Google Standard Roadmap";
	private static final String GOOGLE_TERRAIN = "Google Terrain";
	private static final String GOOGLE_SOMEHOW_ALTERED_ROADMAP = "Google Somehow Altered Roadmap";
	private static final String GOOGLE_SATELLITE_ONLY = "Google Satellite Only";
	private static final String GOOGLE_TERRAIN_ONLY = "Google Terrain Only";
	private static final String GOOGLE_HYBRID = "Google Hybrid";
	private static final String LANDSCAPE = "Landscape Maps";
	private static final String MAPPY_PHOTO = "Mappy Photo Maps";
	private static final String MAPPY_STANDARD = "Mappy Standard Maps";
	private static final String GEO_DATA_INFO = "Geographical Data Information";
	private static final Image WAITING_TILE = new Image(LoadPackageStream.getStream("/waiting_tile.png"));
	private static final Image ERROR_TILE = new Image(LoadPackageStream.getStream("/error_tile.png"));

	private boolean filterROI = false;
	private boolean filterBorder = true;
	private boolean filterGrid = true;
	private boolean filterMapnik;
	private boolean filterOSMCycleMap;
	private boolean filterBingAerialMaps;
	private boolean filterMapQuestOSM;
	private boolean filterMapquestOpenAerial;
	private boolean filterGoogleRoadsOnly;
	private boolean filterGoogleStandardRoadmap;
	private boolean filterGoogleTerrain;
	private boolean filterGoogleSomehowAlteredRoadmap;
	private boolean filterGoogleSatelliteOnly;
	private boolean filterGoogleTerrainOnly;
	private boolean filterGoogleHybrid;
	private boolean filterLandscapeMaps;
	private boolean filterMappyPhoto;
	private boolean filterMappyStandard;
	private ColorProvider colorProvider = new ColorProvider();
	private boolean filterGeoDataInfo;
	@PropertyInfo(index = 0, info = "Zoom level")
	@NumberInfo(min = 0, max = MAX_MERCATOR_TILE_ZOOM, controlType = ControlType.SPINNER)
	private int zoom = 15;
	@PropertyInfo(index = 1, info = "Center the view around the coordinate of the scenario for each new data")
	private boolean followCenterPoint = true;
	@PropertyInfo(index = 2, info = "Center point of the view")
	private GeographicCoordinate centerPoint;
	@PropertyInfo(index = 3, info = "Display tile loadings errors message")
	private boolean displayTileLoadingMessageError;
	private TileSource tileSource;
	private final ConcurrentHashMap<TileId, TileImage> loadingImg = new ConcurrentHashMap<>();
	private final HashMap<Integer, HashMap<Integer, TileImage>> cache = new HashMap<>();
	private boolean dragg;
	private GeographicCoordinate anchorPointGeo;
	private Point2d anchorPoint;
	private final Point2d mousTruePos = new Point2d();
	private final GraphicsContext gc;
	private final GraphicsContext gcInfo;
	private GeographicCoordinate oldCoord;

	private int maxTile;

	public GeographicalDrawer() {
		Canvas canvas = new Canvas();
		canvas.widthProperty().bind(widthProperty());
		canvas.heightProperty().bind(heightProperty());
		this.gc = canvas.getGraphicsContext2D();
		Canvas infoCanvas = new Canvas();
		infoCanvas.setManaged(false);
		infoCanvas.widthProperty().bind(widthProperty());
		infoCanvas.heightProperty().bind(heightProperty());
		infoCanvas.setFocusTraversable(false);
		infoCanvas.setPickOnBounds(false);
		this.gcInfo = infoCanvas.getGraphicsContext2D();
		getChildren().addAll(canvas, infoCanvas);
		setOnMouseDragged(this::onMouseDragged);
		setOnMouseReleased(this::onMouseReleased);
		if (this.tileSource == null)
			setTileSource(new MappyPhotoTileSource());
		updateMaxTile();
	}

	@Override
	protected void onScroll(ScrollEvent e) {
		if (e.isControlDown() && e.getDeltaY() != 0) {
			zoom(e.getDeltaY() < 0, e.getX(), e.getY());
			e.consume();
		}
		super.onScroll(e);
	}

	@Override
	protected void onMousePressed(MouseEvent e) {
		super.onMousePressed(e);
		if (e.isConsumed())
			return;
		MouseButton button = e.getButton();
		if (button == MouseButton.SECONDARY) {
			this.anchorPoint = new Point2d(e.getX(), e.getY());
			this.anchorPointGeo = screenToGeo(new Point2D(getWidth() / 2, getHeight() / 2));
		}
	}

	protected void onMouseDragged(MouseEvent e) {
		if (e.isConsumed())
			return;
		MouseButton button = e.getButton();
		if (button == MouseButton.SECONDARY) {
			if (!this.dragg) {
				setCursor(Cursor.MOVE);
				this.dragg = true;
			}
			double coordX = OsmMercator.MERCATOR_256.lonToX(this.anchorPointGeo.longitude, this.zoom);
			double coordY = OsmMercator.MERCATOR_256.latToY(this.anchorPointGeo.latitude, this.zoom);
			coordX -= e.getX() - this.anchorPoint.x;
			coordY -= e.getY() - this.anchorPoint.y;
			this.centerPoint = new GeographicCoordinate(OsmMercator.MERCATOR_256.yToLat(coordY, this.zoom), OsmMercator.MERCATOR_256.xToLon(coordX, this.zoom));
			repaint(false);
			e.consume();
		}
	}

	protected void onMouseReleased(MouseEvent e) {
		if (e.isConsumed())
			return;
		MouseButton button = e.getButton();
		if (button == MouseButton.SECONDARY) {
			if (this.dragg) {
				this.dragg = false;
				setCursor(Cursor.DEFAULT);
			}
			setCursor(Cursor.DEFAULT);
			this.anchorPoint = null;
			e.consume();
		}
	}

	@Override
	protected void onMouseMoved(MouseEvent e) {
		this.mousTruePos.x = (int) e.getX();
		this.mousTruePos.y = (int) e.getY();
		super.onMouseMoved(e);
	}

	private void setTileSource(TileSource tileSource) {
		this.tileSource = tileSource;
		updateZoom();
		clearImageCache();
		File cacheTilePath = new File(ScenariumProperties.get().getMapTempPath() + tileSource.getPrefix());
		if (!cacheTilePath.exists())
			cacheTilePath.mkdirs();
	}

	@Override
	public void initialize(ScenariumContainer container, Scheduler scheduler, Object dataElement, boolean autoFitIfResize) {
		super.initialize(container, scheduler, dataElement, autoFitIfResize);
		if (this.followCenterPoint) {
			GeographicCoordinate newCoord = getCoordinate();
			if (newCoord != null)
				this.centerPoint = newCoord;
		}
	}

	@Override
	public void dispose() {
		super.dispose();
		clearImageCache();
	}

	@Override
	public boolean isValidAdditionalInput(Class<?>[] inputsType, Class<?> additionalInput) {
		return additionalInput == null ? false
				: GeographicCoordinate.class.isAssignableFrom(additionalInput) || GeographicalDrawableObject2D.class.isAssignableFrom(additionalInput)
						|| GeographicalDrawableObject2D[].class.isAssignableFrom(additionalInput);
	}

	@Override
	public boolean canAddInputToRenderer(Class<?>[] class1) {
		return true;
	}

	@Override
	public void fitDocument() {
		repaint(false);
	}

	@Override
	public Dimension2D getDimension() {
		return new Dimension2D(800, 600);
	}

	@Override
	public String[] getStatusBarInfo() {
		GeographicCoordinate geopPos = screenToGeo(new Point2D(this.mousTruePos.x, this.mousTruePos.y));
		// double[] cartoPos =
		// EarthProjectionLambert93WGS84.geoToCarto(Math.toRadians(geopPos.latitude),
		// Math.toRadians(geopPos.longitude));
		return new String[] { /* "x: " + String.format("%f", cartoPos[0]) + "m", "y: " + String.format("%f", cartoPos[1]) + "m", */ "lat: " + geopPos.latitude + "°", "lon: " + geopPos.longitude + "°",
				"Zoom: " + this.zoom, "TileX: " + (int) OsmMercator.MERCATOR_256.lonToX(geopPos.longitude, this.zoom) / OsmMercator.DEFAULT_TILE_SIZE,
				"TileY: " + (int) OsmMercator.MERCATOR_256.latToY(geopPos.latitude, this.zoom) / OsmMercator.DEFAULT_TILE_SIZE };
	}

	@Override
	public void populateExclusiveFilter(ArrayList<List<String>> exclusiveFilters) {
		exclusiveFilters.add(List.of(MAPNIK, OSM_CYCLE_MAPS, BING_AERIAL_MAPS, MAPQUEST_OSM, MAPQUEST_OPEN_AERIAL, GOOGLE_ROADS_SONLY, GOOGLE_STANDARD_ROADMAP, GOOGLE_TERRAIN,
				GOOGLE_SOMEHOW_ALTERED_ROADMAP, GOOGLE_SATELLITE_ONLY, GOOGLE_TERRAIN_ONLY, GOOGLE_HYBRID, LANDSCAPE, MAPPY_STANDARD, MAPPY_PHOTO));
	}

	@Override
	public boolean updateFilterWithPath(String[] filterPath, boolean value) {
		String filterName = filterPath[filterPath.length - 1];
		if (filterName.equals(GRID))
			this.filterGrid = value;
		else if (filterName.equals(BORDER))
			this.filterBorder = value;
		else if (filterName.equals(MAPNIK)) {
			this.filterMapnik = value;
			updateTileSource();
		} else if (filterName.equals(OSM_CYCLE_MAPS)) {
			this.filterOSMCycleMap = value;
			updateTileSource();
		} else if (filterName.equals(BING_AERIAL_MAPS)) {
			this.filterBingAerialMaps = value;
			updateTileSource();
		} else if (filterName.equals(MAPQUEST_OSM)) {
			this.filterMapQuestOSM = value;
			updateTileSource();
		} else if (filterName.equals(MAPQUEST_OPEN_AERIAL)) {
			this.filterMapquestOpenAerial = value;
			updateTileSource();
		} else if (filterName.equals(GOOGLE_ROADS_SONLY)) {
			this.filterGoogleRoadsOnly = value;
			updateTileSource();
		} else if (filterName.equals(GOOGLE_STANDARD_ROADMAP)) {
			this.filterGoogleStandardRoadmap = value;
			updateTileSource();
		} else if (filterName.equals(GOOGLE_TERRAIN)) {
			this.filterGoogleTerrain = value;
			updateTileSource();
		} else if (filterName.equals(GOOGLE_SOMEHOW_ALTERED_ROADMAP)) {
			this.filterGoogleSomehowAlteredRoadmap = value;
			updateTileSource();
		} else if (filterName.equals(GOOGLE_SATELLITE_ONLY)) {
			this.filterGoogleSatelliteOnly = value;
			updateTileSource();
		} else if (filterName.equals(GOOGLE_TERRAIN_ONLY)) {
			this.filterGoogleTerrainOnly = value;
			updateTileSource();
		} else if (filterName.equals(GOOGLE_HYBRID)) {
			this.filterGoogleHybrid = value;
			updateTileSource();
		} else if (filterName.equals(LANDSCAPE)) {
			this.filterLandscapeMaps = value;
			updateTileSource();
		} else if (filterName.equals(MAPPY_STANDARD)) {
			this.filterMappyStandard = value;
			updateTileSource();
		} else if (filterName.equals(MAPPY_PHOTO)) {
			this.filterMappyPhoto = value;
			updateTileSource();
		} else if (filterName.equals(GEO_DATA_INFO))
			this.filterGeoDataInfo = value;
		/* else if (filterName.equals(FITTOMARKER)) { filterFitToMarker = value; if (filterFitToMarker && map != null) map.setDisplayToFitMapMarkers(); } else if (filterName.equals(INFO)) { filterInfo
		 * = value; repaint(); } */ else
			return false;
		repaint(false);
		return true;
	}

	public static void drawPose(GraphicsContext gc, Function<GeographicCoordinate, Point2D> geoToScreen, double zoom, Color color, GeographicCoordinate coordinate) {
		Point2D screenPos = geoToScreen.apply(coordinate);
		double[] posVar = coordinate.posVar;
		if (posVar != null)
			if (posVar[1] == 0 && posVar[2] == 0) {
				gc.setFill(new Color(0.5, 0.5, 0.5, 0.5));
				double w = zoom * posVar[0];
				double h = zoom * posVar[3];
				double x = screenPos.getX() - w / 2.0;
				double y = screenPos.getY() - h / 2.0;
				gc.fillOval(x, y, w, h); // lon lat
				gc.setStroke(color.darker());
				gc.strokeOval(x, y, w, h);
			} else {
				EigenDecomposition_F64<DMatrixRMaj> eigenDecomposition = DecompositionFactory_DDRM.eig(3, true);
				DMatrix2x2 mat = new DMatrix2x2(posVar[0], posVar[1], posVar[2], posVar[3]);
				eigenDecomposition.decompose(new DMatrixRMaj(mat));
				double sigma = 1;
				double axesWidth = zoom * Math.sqrt(eigenDecomposition.getEigenvalue(1).real) * sigma;
				double axesHeight = zoom * Math.sqrt(eigenDecomposition.getEigenvalue(0).real) * sigma;
				DMatrixRMaj v = eigenDecomposition.getEigenVector(0);
				double angle = Math.atan2(v.get(1, 0), v.get(0, 0));
				Affine transform = gc.getTransform();
				Affine oldTranform = transform.clone();
				transform.appendTranslation(screenPos.getX(), screenPos.getY());
				transform.appendRotation(Math.toDegrees(-angle) - 90);
				gc.setTransform(transform);
				gc.setFill(new Color(0.5, 0.5, 0.5, 0.5));
				gc.fillOval(-axesWidth / 2.0, -axesHeight / 2.0, axesWidth, axesHeight); // lon lat
				gc.setStroke(color.darker());
				gc.strokeOval(-axesWidth / 2.0, -axesHeight / 2.0, axesWidth, axesHeight);
				gc.setTransform(oldTranform);
			}
		gc.setFill(color);
		double radius = 10;
		gc.fillOval(screenPos.getX() - radius / 2, screenPos.getY() - radius / 2, radius, radius);
		gc.setStroke(Color.BLACK);
		gc.strokeOval(screenPos.getX() - radius / 2, screenPos.getY() - radius / 2, radius, radius);
		if (coordinate instanceof GeographicalPose) {
			double lineLength = 20;
			double headingRad = Math.toRadians(((GeographicalPose) coordinate).heading);
			gc.strokeLine(screenPos.getX(), screenPos.getY(), screenPos.getX() + Math.sin(headingRad) * lineLength, screenPos.getY() - Math.cos(headingRad) * lineLength);
		}

	}

	public void drawPose(Color color, GeographicCoordinate coord) {
		GeographicalDrawer.drawPose(this.gc, this::geoToScreen, 1 / getMeterPerPixel(), color, coord);
	}

	protected GraphicsContext getInformationGraphicsContext() {
		return this.gcInfo;
	}

	protected GraphicsContext getGraphicsContext() {
		return this.gc;
	}

	public boolean isFilterGrid() {
		return this.filterGrid;
	}

	public boolean isFilterBorder() {
		return this.filterBorder;
	}

	public boolean isFilterROI() {
		return this.filterROI;
	}

	public void setFilterROI(boolean filterROI) {
		this.filterROI = filterROI;
	}

	public int getZoom() {
		return this.zoom;
	}

	public void setZoom(int zoom) {
		var oldValue = this.zoom;
		if (this.tileSource != null && zoom >= Math.min(this.tileSource.getMaxZoom() + 10, MAX_MERCATOR_TILE_ZOOM))
			zoom = Math.min(this.tileSource.getMaxZoom() + 10, MAX_MERCATOR_TILE_ZOOM);
		else if (zoom <= 0)
			zoom = 0;
		this.zoom = zoom;
		clearImageCache();
		updateMaxTile();
		repaint(false);
		this.pcs.firePropertyChange("zoom", oldValue, this.zoom);
	}

	private void updateMaxTile() {
		this.maxTile = (int) OsmMercator.MERCATOR_256.lonToX(180, this.zoom) / OsmMercator.DEFAULT_TILE_SIZE;
	}

	public boolean isFollowCenterPoint() {
		return this.followCenterPoint;
	}

	public void setFollowCenterPoint(boolean followCenterPoint) {
		var oldValue = this.followCenterPoint;
		this.followCenterPoint = followCenterPoint;
		repaint(false);
		this.pcs.firePropertyChange("followCenterPoint", oldValue, this.followCenterPoint);
	}

	public GeographicCoordinate getCenterPoint() {
		return this.centerPoint;
	}

	public void setCenterPoint(GeographicCoordinate centerPoint) {
		var oldValue = this.centerPoint;
		this.centerPoint = centerPoint;
		repaint(false);
		this.pcs.firePropertyChange("centerPoint", oldValue, this.centerPoint);
	}

	public boolean isDisplayTileLoadingMessageError() {
		return this.displayTileLoadingMessageError;
	}

	public void setDisplayTileLoadingMessageError(boolean displayTileLoadingMessageError) {
		var oldValue = this.displayTileLoadingMessageError;
		this.displayTileLoadingMessageError = displayTileLoadingMessageError;
		this.pcs.firePropertyChange("displayTileLoadingMessageError", oldValue, this.displayTileLoadingMessageError);
	}

	@ArrayInfo(prefHeight = 26 * 8 + 2)
	@PropertyInfo(index = 4, nullable = false, info = "Color for the additional input point cloud")
	public Color[] getChartsColor() {
		return this.colorProvider.getColors();
	}

	public void setChartsColor(Color[] chartsColor) {
		var oldValue = getChartsColor();
		this.colorProvider = new ColorProvider(chartsColor);
		repaint(false);
		this.pcs.firePropertyChange("chartsColor", oldValue, chartsColor);
	}

	@Override
	protected void paint(Object dataElement) {
		GeographicCoordinate newCoord = getCoordinate();
		if (this.followCenterPoint && newCoord != null && !newCoord.equals(this.oldCoord))
			this.centerPoint = newCoord;
		GraphicsContext g = this.gc;
		g.setFill(Color.WHITE);
		g.fillRect(0, 0, getWidth(), getHeight());
		drawTiles();
		this.colorProvider.resetIndex();
		drawPose(Color.YELLOW, getCoordinate());

		if (this.filterGeoDataInfo) {
			ArrayList<String> infos = new ArrayList<>();
			infos.add("latitude:\t" + newCoord.latitude + (newCoord.posVar != null && newCoord.posVar[1] == 0 && newCoord.posVar[2] == 0 ? "\t+-" + newCoord.posVar[0] : ""));
			infos.add("longitude:\t" + newCoord.longitude + (newCoord.posVar != null && newCoord.posVar[1] == 0 && newCoord.posVar[2] == 0 ? "\t+-" + newCoord.posVar[3] : ""));
			if (!Double.isNaN(newCoord.altitude))
				infos.add("altitude:\t" + newCoord.altitude + (!Double.isNaN(newCoord.altitude) ? "\t+-" + newCoord.stdAltitude : ""));
			if (newCoord instanceof GeographicalPose) {
				GeographicalPose newPose = (GeographicalPose) newCoord;
				if (!Double.isNaN(newPose.heading))
					infos.add("heading:\t" + newPose.heading + (!Double.isNaN(newPose.stdHeading) ? "\t+-" + newPose.stdHeading : ""));
				if (!Double.isNaN(newPose.roll))
					infos.add("roll:  \t" + newPose.roll + (!Double.isNaN(newPose.stdRoll) ? "\t+-" + newPose.stdRoll : ""));
				if (!Double.isNaN(newPose.altitude))
					infos.add("pitch:\t" + newPose.pitch + (!Double.isNaN(newPose.stdPitch) ? "\t+-" + newPose.stdPitch : ""));
			}
			double height = new Text("").getLayoutBounds().getHeight();
			double maxWidth = 0;
			for (String info : infos) {
				double width = new Text(info).getLayoutBounds().getWidth();
				if (width > maxWidth)
					maxWidth = width;
			}
			this.gcInfo.setFill(new Color(1, 1, 1, 0.6));
			this.gcInfo.fillRect(0, 0, maxWidth + 10, infos.size() * height + 8);
			this.gcInfo.setFill(Color.BLACK);
			for (int i = 0; i < infos.size(); i++)
				this.gcInfo.fillText(infos.get(i), 5, height + i * height);
		}
		ArrayList<GeographicalDrawableObject2D> geoMarkers = getGeoMarkers();
		if (geoMarkers != null)
			for (GeographicalDrawableObject2D geoMarker : geoMarkers)
				if (geoMarker != null)
					geoMarker.draw(this.gcInfo, this::geoToScreen);
		ArrayList<GeographicalPose> otherPoses = getOtherPoses();
		if (otherPoses != null)
			for (GeographicalPose pose : otherPoses) {
				this.colorProvider.incrementColorIndex();
				Color c = this.colorProvider.getNextColor();
				drawPose(c, pose);
			}
		this.oldCoord = newCoord;
	}

	protected void drawTiles() {
		GraphicsContext g = this.gc;
		double coordX = OsmMercator.MERCATOR_256.lonToX(this.centerPoint.longitude, this.zoom) / OsmMercator.DEFAULT_TILE_SIZE; // je
		// suis
		// en
		// tuille
		double coordY = OsmMercator.MERCATOR_256.latToY(this.centerPoint.latitude, this.zoom) / OsmMercator.DEFAULT_TILE_SIZE;
		int beginX = (int) coordX;
		int beginY = (int) coordY;
		int xCenter = (int) (getWidth() / 2) - (int) ((coordX - beginX) * OsmMercator.DEFAULT_TILE_SIZE);
		int yCenter = (int) (getHeight() / 2) - (int) ((coordY - beginY) * OsmMercator.DEFAULT_TILE_SIZE);
		int beginTileX = -(xCenter + OsmMercator.DEFAULT_TILE_SIZE - 1) / OsmMercator.DEFAULT_TILE_SIZE;
		int beginTileY = -(yCenter + OsmMercator.DEFAULT_TILE_SIZE - 1) / OsmMercator.DEFAULT_TILE_SIZE;
		int endTileX = ((int) getWidth() - (xCenter + OsmMercator.DEFAULT_TILE_SIZE) + OsmMercator.DEFAULT_TILE_SIZE - 1) / OsmMercator.DEFAULT_TILE_SIZE + 1;
		int endTileY = ((int) getHeight() - (yCenter + OsmMercator.DEFAULT_TILE_SIZE) + OsmMercator.DEFAULT_TILE_SIZE - 1) / OsmMercator.DEFAULT_TILE_SIZE + 1;
		int idScale = this.zoom;
		boolean upScaling = this.zoom > this.tileSource.getMaxZoom();
		int deltaZoom = 0;
		if (upScaling) {
			idScale = this.tileSource.getMaxZoom();
			deltaZoom = (int) Math.pow(2, this.zoom - this.tileSource.getMaxZoom());
		}
		int minX = beginX + beginTileX;
		int maxX = beginX + endTileX;
		int minY = beginY + beginTileY;
		int maxY = beginY + endTileY;

		// Cancel des anciennes tuiles
		for (TileId tileId : this.loadingImg.keySet()) {
			TileImage tileImage = this.loadingImg.get(tileId);
			tileImage.usedCpt = 0;
			if (tileId.scale != idScale || tileId.x < minX || tileId.x > maxX || tileId.y < minY || tileId.y > maxY) {
				tileImage.image.cancel();
				tileImage.isCancel = true;
				this.loadingImg.remove(tileId);
			} else { // Je garde et recalcul la position
				int i = tileId.x - beginX;
				int j = tileId.y - beginY;
				int xc = xCenter + i * OsmMercator.DEFAULT_TILE_SIZE;
				int yc = yCenter + j * OsmMercator.DEFAULT_TILE_SIZE;
				tileImage.xDraw = xc;
				tileImage.yDraw = yc;
			}
		}
		this.cache.values().forEach(a -> a.values().forEach(b -> b.usedCpt = 0));
		for (int i = beginTileX; i < endTileX; i++)
			for (int j = beginTileY; j < endTileY; j++) {
				int idX = beginX + i;
				int idY = beginY + j;
				if (idX < 0 || idY < 0 || idX >= this.maxTile || idY >= this.maxTile)
					continue;
				if (upScaling) {
					idX /= deltaZoom;
					idY /= deltaZoom;
				}
				int xc = xCenter + i * OsmMercator.DEFAULT_TILE_SIZE;
				int yc = yCenter + j * OsmMercator.DEFAULT_TILE_SIZE;
				// Chargement à partir du cache ram
				HashMap<Integer, TileImage> idc = this.cache.get(idX);
				if (idc != null) {
					TileImage img = idc.get(idY);
					if (img != null)
						if (img.isCancel)
							idc.remove(idY);
						else {
							// Log.info("charge du cache: " + idX + " " + idY);
							img.usedCpt++;
							TileImage tileImage = img;
							tileImage.xDraw = xc;
							tileImage.yDraw = yc;
							drawImage(g, tileImage, upScaling, beginX + i, beginY + j, deltaZoom);
							continue;
						}
				}
				// Chargement à partir des images en cours de chargement
				TileImage tileImage = this.loadingImg.get(new TileId(idX, idY, idScale));
				if (tileImage != null) {
					if (tileImage.usedCpt == 0) {
						tileImage.xDraw = xc;
						tileImage.yDraw = yc;
						drawImage(g, tileImage, upScaling, beginX + i, beginY + j, deltaZoom);
					} else {
						tileImage.addPlaceToDraw(xc, yc, beginX + i, beginY + j, deltaZoom);
						drawImage(g, new TileImage(tileImage.image, xc, yc), upScaling, beginX + i, beginY + j, deltaZoom);
					}
					continue;
				}
				// Chargement à partir du cache disque dur
				File imgFile = new File(ScenariumProperties.get().getMapTempPath() + this.tileSource.getPrefix() + "/" + idScale + "_" + idX + "_" + idY + ".png");
				if (imgFile.exists()) {
					tileImage = new TileImage(new Image(imgFile.toURI().toString()), xc, yc);
					if (!tileImage.image.isError())
						putImageInCache(idX, idY, tileImage);
					drawImage(g, tileImage, upScaling, beginX + i, beginY + j, deltaZoom);
					// Log.info("charge du disque dur: " + idX + " " + idY);
				} else
					try {
						// Chargement à partir d'internet
						// Log.info("charge d'internet: " + idX + " " + idY);
						TileImage loadingTileImage = new TileImage(new Image(this.tileSource.getTileUrl(idScale, idX, idY), true), xc, yc);
						TileId tileId = new TileId(idX, idY, idScale);
						this.loadingImg.put(tileId, loadingTileImage);
						drawImage(g, loadingTileImage, upScaling, beginX + i, beginY + j, deltaZoom);
						int idXC = idX;
						int idyC = idY;
						int iC = i;
						int jC = j;
						int deltaZoomC = deltaZoom;
						loadingTileImage.image.progressProperty().addListener((a, b, progress) -> {
							if ((Double) progress == 1.0) {
								if (loadingTileImage.image.getException() instanceof CancellationException)
									return;
								drawImage(g, loadingTileImage, upScaling, beginX + iC, beginY + jC, deltaZoomC);
								if (loadingTileImage.additionalPlaceToDraw != null)
									for (int[] p : loadingTileImage.additionalPlaceToDraw) {
										loadingTileImage.xDraw = p[0];
										loadingTileImage.yDraw = p[1];
										drawImage(g, loadingTileImage, upScaling, p[2], p[3], p[4]);
									}
								if (!loadingTileImage.image.isError()) {
									putImageInCache(idXC, idyC, loadingTileImage);
									new Thread(() -> {
										BufferedImage bImage = SwingFXUtils.fromFXImage(loadingTileImage.image, null);
										try {
											ImageIO.write(bImage, "png", imgFile);
											this.loadingImg.remove(tileId);
										} catch (Exception e) {
											e.printStackTrace();
										}
									}).start();
								} else if (this.displayTileLoadingMessageError)
									Log.error("" + loadingTileImage.image.getException());
							}
						});
					} catch (IOException ex) {
						g.drawImage(ERROR_TILE, xc, yc);
						if (this.displayTileLoadingMessageError)
							Log.error("" + ex);
					}
			}

		ArrayList<Integer> toRemoveX = new ArrayList<>();
		ArrayList<Integer> toRemoveY = new ArrayList<>();
		this.cache.forEach((idX, idXs) -> {
			toRemoveY.clear();
			idXs.forEach((idY, idYs) -> {
				if (idYs.usedCpt == 0)
					toRemoveY.add(idY);
			});
			for (Integer idY : toRemoveY)
				idXs.remove(idY);
			if (idXs.isEmpty())
				toRemoveX.add(idX);
		});
		for (Integer idX : toRemoveX)
			this.cache.remove(idX);

		this.gcInfo.clearRect(0, 0, getWidth(), getHeight());
		if (this.filterGrid) {
			this.gcInfo.setStroke(Color.BLACK);
			int end = xCenter + endTileX * OsmMercator.DEFAULT_TILE_SIZE;
			for (int i = xCenter + beginTileX * OsmMercator.DEFAULT_TILE_SIZE; i <= end; i += OsmMercator.DEFAULT_TILE_SIZE)
				this.gcInfo.strokeLine(i + 0.5, 0 + 0.5, i + 0.5, getHeight() + 0.5);
			end = yCenter + endTileY * OsmMercator.DEFAULT_TILE_SIZE;
			for (int i = yCenter + beginTileY * OsmMercator.DEFAULT_TILE_SIZE; i <= end; i += OsmMercator.DEFAULT_TILE_SIZE)
				this.gcInfo.strokeLine(0 + 0.5, i + 0.5, getWidth() + 0.5, i + 0.5);
		}
	}

	protected ArrayList<GeographicalPose> getOtherPoses() {
		Object[] add = getAdditionalDrawableElement();
		if (add == null)
			return null;
		ArrayList<GeographicalPose> geoPoses = new ArrayList<>();
		for (Object addInput : add)
			if (addInput instanceof GeographicalPose)
				geoPoses.add((GeographicalPose) addInput);
		return geoPoses;
	}

	protected ArrayList<GeographicalDrawableObject2D> getGeoMarkers() {
		Object[] add = getAdditionalDrawableElement();
		if (add == null)
			return null;
		ArrayList<GeographicalDrawableObject2D> geoMarkers = new ArrayList<>();
		for (Object addInput : add)
			if (addInput instanceof GeographicalDrawableObject2D[])
				for (GeographicalDrawableObject2D o : (GeographicalDrawableObject2D[]) addInput)
					geoMarkers.add(o);
			else if (addInput instanceof GeographicalDrawableObject2D)
				geoMarkers.add((GeographicalDrawableObject2D) addInput);
		return geoMarkers;
	}

	protected double getMeterPerPixel() {
		Point2D origin = new Point2D(5, 5);
		Point2D center = new Point2D(getWidth() / 2, getHeight() / 2);
		double pDistance = center.distance(origin);
		GeographicCoordinate originCoord = screenToGeo(origin);
		GeographicCoordinate centerCoord = screenToGeo(center);
		double mDistance = OsmMercator.MERCATOR_256.getDistance(originCoord.latitude, originCoord.longitude, centerCoord.latitude, centerCoord.longitude);
		return mDistance / pDistance;
	}

	protected GeographicCoordinate getOldCoord() {
		return this.oldCoord;
	}

	protected GeographicCoordinate cartoToGeo(Point2D pointXY) {
		return new GeographicCoordinate(OsmMercator.MERCATOR_256.yToLat(pointXY.getY(), this.zoom), OsmMercator.MERCATOR_256.xToLon(pointXY.getX(), this.zoom));
	}

	protected Point2D geoToCarto(GeographicCoordinate geo) {
		return new Point2D(OsmMercator.MERCATOR_256.lonToX(geo.longitude, this.zoom), OsmMercator.MERCATOR_256.latToY(geo.latitude, this.zoom));
	}

	protected Point2D geoToScreen(GeographicCoordinate geo) {
		double coordX = OsmMercator.MERCATOR_256.lonToX(geo.longitude, this.zoom) - OsmMercator.MERCATOR_256.lonToX(this.centerPoint.longitude, this.zoom) + getWidth() / 2; // je suis en tuille
		double coordY = OsmMercator.MERCATOR_256.latToY(geo.latitude, this.zoom) - OsmMercator.MERCATOR_256.latToY(this.centerPoint.latitude, this.zoom) + getHeight() / 2; // je suis en tuille
		return new Point2D(coordX, coordY);
	}

	protected GeographicCoordinate getCoordinate() {
		return (GeographicCoordinate) getDrawableElement();
	}

	protected Point2D screenToCarto(Point2D point) {
		double coordX = OsmMercator.MERCATOR_256.lonToX(this.centerPoint.longitude, this.zoom) - (getWidth() / 2 - point.getX());
		double coordY = OsmMercator.MERCATOR_256.latToY(this.centerPoint.latitude, this.zoom) - (getHeight() / 2 - point.getY());
		return new Point2D(coordX, coordY);
	}

	protected GeographicCoordinate screenToGeo(Point2D point2d) {
		return cartoToGeo(screenToCarto(point2d));
	}

	@Override
	protected void populateTheaterFilter(TreeRoot<BooleanProperty> theaterFilter) {
		super.populateTheaterFilter(theaterFilter);
		TreeNode<BooleanProperty> filtersMap = new TreeNode<>(new BooleanProperty(GENERAL_FILTERS, true));
		filtersMap.addChild(new TreeNode<>(new BooleanProperty(BORDER, true)));
		filtersMap.addChild(new TreeNode<>(new BooleanProperty(GRID, true)));
		theaterFilter.addChild(filtersMap);
		filtersMap = new TreeNode<>(new BooleanProperty("Maps", true));
		filtersMap.addChild(new TreeNode<>(new BooleanProperty(MAPNIK, false)));
		filtersMap.addChild(new TreeNode<>(new BooleanProperty(OSM_CYCLE_MAPS, false)));
		filtersMap.addChild(new TreeNode<>(new BooleanProperty(BING_AERIAL_MAPS, false)));
		filtersMap.addChild(new TreeNode<>(new BooleanProperty(MAPQUEST_OSM, false)));
		filtersMap.addChild(new TreeNode<>(new BooleanProperty(MAPQUEST_OPEN_AERIAL, false)));
		filtersMap.addChild(new TreeNode<>(new BooleanProperty(GOOGLE_ROADS_SONLY, false)));
		filtersMap.addChild(new TreeNode<>(new BooleanProperty(GOOGLE_STANDARD_ROADMAP, false)));
		filtersMap.addChild(new TreeNode<>(new BooleanProperty(GOOGLE_TERRAIN, false)));
		filtersMap.addChild(new TreeNode<>(new BooleanProperty(GOOGLE_SOMEHOW_ALTERED_ROADMAP, false)));
		filtersMap.addChild(new TreeNode<>(new BooleanProperty(GOOGLE_SATELLITE_ONLY, false)));
		filtersMap.addChild(new TreeNode<>(new BooleanProperty(GOOGLE_TERRAIN_ONLY, false)));
		filtersMap.addChild(new TreeNode<>(new BooleanProperty(GOOGLE_HYBRID, false)));
		filtersMap.addChild(new TreeNode<>(new BooleanProperty(LANDSCAPE, false)));
		filtersMap.addChild(new TreeNode<>(new BooleanProperty(MAPPY_PHOTO, true)));
		filtersMap.addChild(new TreeNode<>(new BooleanProperty(MAPPY_STANDARD, false)));
		theaterFilter.addChild(filtersMap);
		theaterFilter.addChild(new TreeNode<>(new BooleanProperty(GEO_DATA_INFO, false)));
	}

	private static void drawImage(GraphicsContext g, TileImage tileImage, boolean upScaling, int i, int j, int deltaZoom) {
		if (tileImage == null || tileImage.isCancel)
			Log.error("cannot drawImage, the image is null or cancelled");
		Image image = tileImage.image.isError() ? ERROR_TILE : tileImage.image.getProgress() != 1 ? WAITING_TILE : tileImage.image;
		if (upScaling) {
			int deltaX = i % deltaZoom == 0 ? 0 : OsmMercator.DEFAULT_TILE_SIZE * (i % deltaZoom) / deltaZoom;
			int deltaY = j % deltaZoom == 0 ? 0 : OsmMercator.DEFAULT_TILE_SIZE * (j % deltaZoom) / deltaZoom;
			g.drawImage(image, deltaX, deltaY, OsmMercator.DEFAULT_TILE_SIZE / deltaZoom, OsmMercator.DEFAULT_TILE_SIZE / deltaZoom, tileImage.xDraw, tileImage.yDraw, 256, 256);
		} else
			g.drawImage(image, tileImage.xDraw, tileImage.yDraw);
	}

	private void putImageInCache(int idX, int idY, TileImage tileImage) {
		HashMap<Integer, TileImage> idca = this.cache.get(idX);
		if (idca == null) {
			idca = new HashMap<>();
			this.cache.put(idX, idca);
		}
		idca.put(idY, tileImage);
	}

	private void clearImageCache() {
		for (TileImage dImg : this.loadingImg.values()) // TODO garder celle que l'on aura encore, map pour la structure
														// les contenant
			dImg.image.cancel();
		this.loadingImg.clear();
		this.cache.clear();
	}

	private void updateZoom() {
		if (this.zoom < this.tileSource.getMinZoom())
			setZoom(this.tileSource.getMinZoom());
		else if (this.zoom > this.tileSource.getMaxZoom())
			setZoom(this.tileSource.getMaxZoom());
	}

	private void zoom(boolean in, double x, double y) {
		int newZoom = this.zoom;
		int minScale = this.tileSource.getMinZoom();
		int maxScale = Math.min(this.tileSource.getMaxZoom() + 10, MAX_MERCATOR_TILE_ZOOM); // 23 Maximum available zoom
		if (in)
			newZoom--;
		else
			newZoom++;
		if (newZoom < minScale)
			newZoom = minScale;
		else if (newZoom > maxScale)
			newZoom = maxScale;
		if (this.zoom != newZoom)
			setZoom(newZoom);
	}

	private void updateTileSource() {
		TileSource tileSource = null;
		if (this.filterMapnik)
			tileSource = new MapnikTileSource();
		else if (this.filterOSMCycleMap)
			tileSource = new CycleMapTileSource();
		else if (this.filterBingAerialMaps)
			tileSource = new BingAerialTileSource();
		else if (this.filterMapQuestOSM)
			tileSource = new MapQuestOSMTileSource();
		else if (this.filterMapquestOpenAerial)
			tileSource = new MapQuestOSMAerialTileSource();
		else if (this.filterGoogleRoadsOnly)
			tileSource = new GoogleRoadsOnlyTileSource();
		else if (this.filterGoogleStandardRoadmap)
			tileSource = new GoogleStandardRoadmapTileSource();
		else if (this.filterGoogleTerrain)
			tileSource = new GoogleTerrainTileSource();
		else if (this.filterGoogleSomehowAlteredRoadmap)
			tileSource = new GoogleSomehowAlteredRoadmapTileSource();
		else if (this.filterGoogleSatelliteOnly)
			tileSource = new GoogleSatelliteOnlyTileSource();
		else if (this.filterGoogleTerrainOnly)
			tileSource = new GoogleTerrainOnlyTileSource();
		else if (this.filterGoogleHybrid)
			tileSource = new GoogleHybridTileSource();
		else if (this.filterLandscapeMaps)
			tileSource = new LandscapeTileSource();
		else if (this.filterMappyStandard)
			tileSource = new MappyStandardTileSource();
		else if (this.filterMappyPhoto)
			tileSource = new MappyPhotoTileSource();
		if (tileSource != null)
			setTileSource(tileSource);
	}

	protected ColorProvider getColorProvider() {
		return this.colorProvider;
	}

	public int getMaxTile() {
		return this.maxTile;
	}

	protected class TileImage {
		public ArrayList<int[]> additionalPlaceToDraw;
		public boolean isCancel;
		public Image image;
		public int xDraw;
		public int yDraw;
		public int usedCpt;

		public TileImage(Image image, int xDraw, int yDraw) {
			this.image = image;
			this.usedCpt = 1;
			this.xDraw = xDraw;
			this.yDraw = yDraw;
		}

		public void addPlaceToDraw(int xc, int yc, int i, int j, int deltaZoom) {
			if (this.additionalPlaceToDraw == null)
				this.additionalPlaceToDraw = new ArrayList<>();
			this.additionalPlaceToDraw.add(new int[] { xc, yc, i, j, deltaZoom });
		}

		@Override
		public String toString() {
			return "";
		}
	}
}
