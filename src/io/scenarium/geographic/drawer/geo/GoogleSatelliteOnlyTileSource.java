/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.geographic.drawer.geo;

public class GoogleSatelliteOnlyTileSource implements TileSource {

	@Override
	public int getMaxZoom() {
		return 21;
	}

	@Override
	public int getMinZoom() {
		return 0;
	}

	@Override
	public String getPrefix() {
		return "GoogleSatelliteOnly";
	}

	@Override
	public String getTileUrl(int zoom, int tilex, int tiley) {
		return "https://mts1.google.com/vt/lyrs=s&x=" + tilex + "&y=" + tiley + "&z=" + zoom;
	}

}
