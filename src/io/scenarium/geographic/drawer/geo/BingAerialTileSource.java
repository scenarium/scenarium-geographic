/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.geographic.drawer.geo;

import java.awt.Image;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import io.scenarium.geographic.internal.LoadPackageStream;
import io.scenarium.geographic.internal.Log;
import io.scenarium.geographic.struct.GeographicCoordinate;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class BingAerialTileSource implements TileSource {
	protected static class Attribution {
		private String attribution;
		private int minZoom;
		private int maxZoom;
		private GeographicCoordinate min;
		private GeographicCoordinate max;
	}

	private static final String API_KEY = "Arzdiw4nlOJzRwOz__qailc8NiR31Tt51dN2D7cm57NrnceZnCpgOkmJhNpGoppU";
	private static volatile List<Attribution> attributions; // volatile is required for getAttribution(), see below.
	private static String imageUrlTemplate;
	private static Integer imageryZoomMax;

	private static String[] subdomains;
	private static final Pattern SUBDOMAINPATTERN = Pattern.compile("\\{subdomain\\}");
	private static final Pattern QUADKEYPATTERN = Pattern.compile("\\{quadkey\\}");
	private static final Pattern CULTUREPATTERN = Pattern.compile("\\{culture\\}");

	private static String computeQuadTree(int zoom, int tilex, int tiley) {
		StringBuilder k = new StringBuilder();
		for (int i = zoom; i > 0; i--) {
			char digit = 48;
			int mask = 1 << i - 1;
			if ((tilex & mask) != 0)
				digit += 1;
			if ((tiley & mask) != 0)
				digit += 2;
			k.append(digit);
		}
		return k.toString();
	}

	private String brandLogoUri = null;

	public BingAerialTileSource() {}

	protected List<Attribution> getAttribution() {
		if (attributions == null)
			// see http://www.cs.umd.edu/~pugh/java/memoryModel/DoubleCheckedLocking.html
			synchronized (BingAerialTileSource.class) {
				if (attributions == null)
					attributions = getAttributionLoaderCallable();
			}
		return attributions;
	}

	public Image getAttributionImage() {
		try {
			final InputStream imageResource = LoadPackageStream.getStream("bing_maps.png");
			if (imageResource != null)
				return ImageIO.read(imageResource);
			else if (this.brandLogoUri != null && !this.brandLogoUri.isEmpty()) {
				Log.info("Reading Bing logo from " + this.brandLogoUri);
				return ImageIO.read(new URL(this.brandLogoUri));
			}
		} catch (IOException e) {
			Log.error("Error while retrieving Bing logo: " + e.getMessage());
		}
		return null;
	}

	public String getAttributionImageURL() {
		return "http://opengeodata.org/microsoft-imagery-details";
	}

	public String getAttributionLinkURL() {
		return "http://go.microsoft.com/?linkid=9710837";
	}

	protected List<Attribution> getAttributionLoaderCallable() {
		try {
			InputSource xml = new InputSource(getAttributionUrl().openStream());
			List<Attribution> r = parseAttributionText(xml);
			Log.info("Successfully loaded Bing attribution data.");
			return r;
		} catch (IOException ex) {
			Log.error("Could not connect to Bing API.");
			return null;
		}
	}

	public String getAttributionText(int zoom, GeographicCoordinate topLeft, GeographicCoordinate botRight) {
		try {
			final List<Attribution> data = getAttribution();
			if (data == null)
				return "Error loading Bing attribution data";
			StringBuilder a = new StringBuilder();
			for (Attribution attr : data)
				if (zoom <= attr.maxZoom && zoom >= attr.minZoom)
					if (topLeft.longitude < attr.max.longitude && botRight.longitude > attr.min.longitude && topLeft.latitude > attr.min.latitude && botRight.latitude < attr.max.latitude) {
						a.append(attr.attribution);
						a.append(' ');
					}
			return a.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "Error loading Bing attribution data";
	}

	protected URL getAttributionUrl() throws MalformedURLException {
		return new URL("http://dev.virtualearth.net/REST/v1/Imagery/Metadata/Aerial?include=ImageryProviders&output=xml&key=" + API_KEY);
	}

	@Override
	public int getMaxZoom() {
		if (imageryZoomMax != null)
			return imageryZoomMax - 2; // -3 sinon tuile interdite
		return 22 - 3; // -3 sinon tuile interdite
	}

	@Override
	public int getMinZoom() {
		return 0;
	}

	@Override
	public String getPrefix() {
		return "Bing";
	}

	public String getTermsOfUseText() {
		return null;
	}

	public String getTermsOfUseURL() {
		return "http://opengeodata.org/microsoft-imagery-details";
	}

	@Override
	public String getTileUrl(int zoom, int tilex, int tiley) throws IOException {
		// make sure that attribution is loaded. otherwise subdomains is null.
		if (getAttribution() == null || tilex < 0 || tiley < 0)
			throw new IOException("Attribution is not loaded yet");

		int t = (zoom + tilex + tiley) % subdomains.length;
		String subdomain = subdomains[t];

		String url = imageUrlTemplate;
		url = SUBDOMAINPATTERN.matcher(url).replaceAll(subdomain);
		url = QUADKEYPATTERN.matcher(url).replaceAll(computeQuadTree(zoom, tilex, tiley));

		return url;
	}

	protected List<Attribution> parseAttributionText(InputSource xml) throws IOException {
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document document = builder.parse(xml);

			XPathFactory xPathFactory = XPathFactory.newInstance();
			XPath xpath = xPathFactory.newXPath();
			imageUrlTemplate = xpath.compile("//ImageryMetadata/ImageUrl/text()").evaluate(document);
			imageUrlTemplate = CULTUREPATTERN.matcher(imageUrlTemplate).replaceAll(Locale.getDefault().toString());
			imageryZoomMax = Integer.valueOf(xpath.compile("//ImageryMetadata/ZoomMax/text()").evaluate(document));

			NodeList subdomainTxt = (NodeList) xpath.compile("//ImageryMetadata/ImageUrlSubdomains/string/text()").evaluate(document, XPathConstants.NODESET);
			subdomains = new String[subdomainTxt.getLength()];
			for (int i = 0; i < subdomainTxt.getLength(); i++)
				subdomains[i] = subdomainTxt.item(i).getNodeValue();

			this.brandLogoUri = xpath.compile("/Response/BrandLogoUri/text()").evaluate(document);

			XPathExpression attributionXpath = xpath.compile("Attribution/text()");
			XPathExpression coverageAreaXpath = xpath.compile("CoverageArea");
			XPathExpression zoomMinXpath = xpath.compile("ZoomMin/text()");
			XPathExpression zoomMaxXpath = xpath.compile("ZoomMax/text()");
			XPathExpression southLatXpath = xpath.compile("BoundingBox/SouthLatitude/text()");
			XPathExpression westLonXpath = xpath.compile("BoundingBox/WestLongitude/text()");
			XPathExpression northLatXpath = xpath.compile("BoundingBox/NorthLatitude/text()");
			XPathExpression eastLonXpath = xpath.compile("BoundingBox/EastLongitude/text()");

			NodeList imageryProviderNodes = (NodeList) xpath.compile("//ImageryMetadata/ImageryProvider").evaluate(document, XPathConstants.NODESET);
			List<Attribution> attributions = new ArrayList<>(imageryProviderNodes.getLength());
			for (int i = 0; i < imageryProviderNodes.getLength(); i++) {
				Node providerNode = imageryProviderNodes.item(i);

				String attribution = attributionXpath.evaluate(providerNode);

				NodeList coverageAreaNodes = (NodeList) coverageAreaXpath.evaluate(providerNode, XPathConstants.NODESET);
				for (int j = 0; j < coverageAreaNodes.getLength(); j++) {
					Node areaNode = coverageAreaNodes.item(j);
					Attribution attr = new Attribution();
					attr.attribution = attribution;

					attr.maxZoom = Integer.parseInt(zoomMaxXpath.evaluate(areaNode));
					attr.minZoom = Integer.parseInt(zoomMinXpath.evaluate(areaNode));

					Double southLat = Double.valueOf(southLatXpath.evaluate(areaNode));
					Double northLat = Double.valueOf(northLatXpath.evaluate(areaNode));
					Double westLon = Double.valueOf(westLonXpath.evaluate(areaNode));
					Double eastLon = Double.valueOf(eastLonXpath.evaluate(areaNode));
					attr.min = new GeographicCoordinate(southLat, westLon);
					attr.max = new GeographicCoordinate(northLat, eastLon);

					attributions.add(attr);
				}
			}

			return attributions;
		} catch (SAXException e) {
			Log.error("Could not parse Bing aerials attribution metadata.");
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		return null;
	}

	public boolean requiresAttribution() {
		return true;
	}
}
