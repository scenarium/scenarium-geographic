/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.geographic.drawer.geo;

import io.scenarium.geographic.struct.GeographicCoordinate;
import io.scenarium.geographic.struct.GeographicalDrawableObject2D;
import io.scenarium.geographic.struct.GeographicalDrawableObject3D;

public abstract class GeographicalMarker implements GeographicalDrawableObject2D, GeographicalDrawableObject3D {
	public final GeographicCoordinate coordinate;

	public GeographicalMarker(GeographicCoordinate coordinate) {
		if (coordinate == null)
			throw new IllegalArgumentException("The coordinate of the " + GeographicalMarker.class + " cannot be null");
		this.coordinate = coordinate;
	}

	public GeographicCoordinate getCoordinate() {
		return this.coordinate;
	}
}
