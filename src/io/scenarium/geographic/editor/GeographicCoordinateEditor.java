/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.geographic.editor;

import java.util.List;
import java.util.function.Supplier;

import io.beanmanager.editors.multiNumber.StaticSizeMonoDimensionEditor;
import io.beanmanager.editors.primitive.number.DoubleEditor;
import io.beanmanager.editors.primitive.number.NumberEditor;
import io.scenarium.geographic.struct.GeographicCoordinate;

public class GeographicCoordinateEditor extends StaticSizeMonoDimensionEditor<GeographicCoordinate, Double> {

	public GeographicCoordinateEditor() {
		super(3);
	}

	@Override
	protected List<Double> getArrayFromValue(GeographicCoordinate value) {
		return List.of(value.latitude, value.longitude, value.altitude);
	}

	@Override
	protected Supplier<NumberEditor<Double>> getEditorConstructor() {
		return DoubleEditor::new;
	}

	@Override
	protected GeographicCoordinate getValueFromArray(List<Double> datas) {
		return new GeographicCoordinate(datas.get(0), datas.get(1), datas.get(2));
	}

	@Override
	protected int[] getDimensions(GeographicCoordinate object) {
		return new int[] { 3 };
	}
}
