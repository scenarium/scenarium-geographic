/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.geographic.editor;

import java.util.List;
import java.util.function.Supplier;

import io.beanmanager.editors.multiNumber.StaticSizeMonoDimensionEditor;
import io.beanmanager.editors.primitive.number.DoubleEditor;
import io.beanmanager.editors.primitive.number.NumberEditor;
import io.scenarium.geographic.struct.GeographicalPose;

public class GeographicalPoseEditor extends StaticSizeMonoDimensionEditor<GeographicalPose, Double> {

	public GeographicalPoseEditor() {
		super(14);
	}

	public GeographicalPoseEditor(int n) {
		super(14);
	}

	@Override
	protected List<Double> getArrayFromValue(GeographicalPose value) {
		return List.of(value.latitude, value.longitude, value.altitude, value.posVar[0], value.posVar[1], value.posVar[2], value.posVar[3], value.stdAltitude, value.heading, value.roll, value.pitch,
				value.stdHeading, value.stdRoll, value.stdPitch);
	}

	@Override
	protected Supplier<NumberEditor<Double>> getEditorConstructor() {
		return DoubleEditor::new;
	}

	@Override
	protected GeographicalPose getValueFromArray(List<Double> datas) {
		return new GeographicalPose(datas.get(0), datas.get(1), datas.get(2), new double[] { datas.get(3), datas.get(4), datas.get(5), datas.get(6) }, datas.get(7), datas.get(8), datas.get(9),
				datas.get(10), datas.get(11), datas.get(12), datas.get(13));
	}

	@Override
	protected int[] getDimensions(GeographicalPose object) {
		return new int[] { 14 };
	}
}
